from flask import Flask
from flask import request
from flask import render_template

import os

#Put all files in templates folder next to app.py
app = Flask(__name__, template_folder='./templates')

@app.route("/")
def hello():
	return render_template('index.html')

@app.route("/<url>")
def mainPageRouter(url):
	pathroot = request.url[21:]
	if '~' in request.url or '..' in request.url or '//' in pathroot:
		return render_template('403.html'),'403'
	else:
		if 'css' in url or 'html' in url:
			urlList = os.listdir('./templates')
			if url in urlList:
				try:
					return render_template(url)
				except OSError as error:
					return render_template('403.html'), '403'
			else:
				return render_template('404.html'),'404'
		else:
			return render_template('404.html'),'404'


@app.errorhandler(403)
def page_forbidden(e):
	return render_template('403.html'),'403'

@app.errorhandler(404)
def page_forbidden(e):
	pathroot = request.url[21:]
	if '~' in request.url or '..' in request.url or '//' in pathroot:
		return render_template('403.html'),'403'
	else:
		return render_template('404.html'),'404'
	

if __name__ == "__main__":
	app.run(debug=True,host='0.0.0.0')
